from pythainlp.tokenize import word_tokenize
from pythainlp.corpus import thai_stopwords


THAI_STOPWORDS = set(thai_stopwords())


def _remove_thai_stopwords(tokens):
    return [token for token in tokens if token not in THAI_STOPWORDS]


def cleaner(text):
    tokens = word_tokenize(text, keep_whitespace=False)
    tokens = _remove_thai_stopwords(tokens)
    return tokens
