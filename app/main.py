from flask import Flask, request, make_response, jsonify
from text_preprocessing import cleaner
import json


app = Flask(__name__)


@app.route('/cleaner', methods=['POST'])
def tokenize_service():
    body = json.loads(request.get_data())
    if "text" not in body:
        response = jsonify({"status": "error",
                            "description": "`text` field not found."})
        return make_response(response, 400)

    text = body["text"]
    result = cleaner(text)
    response = jsonify({"status": "ok",
                        "result": result})
    return make_response(response, 200)


if __name__=="__main__":
    app.run("0.0.0.0", port=5000)
