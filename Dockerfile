FROM python:3.7-slim
EXPOSE 5000

COPY app /app
COPY requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip install -r requirements.txt
CMD ["python", "main"]
