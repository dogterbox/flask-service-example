**Example**

```
curl --request POST \
  --url http://localhost:5000/cleaner \
  --header 'Content-Type: application/json' \
  --data '{
	"text": "ตอนนี้อากาศเริ่มจะหนาวแล้วนะ ฝุ่นก็เยอะด้วย โควิดก็กลับมาระบาดอีกแล้ว"
}'
```
![rest-api](example/rest-api-example.png)
